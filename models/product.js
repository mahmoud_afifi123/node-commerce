var mongoose = require('mongoose');
var productSchema = mongoose.Schema(
    {
        name: {type: String, required: true},
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        price: {type: Number, required: true},
        size: {type: Number, required: true},
        description: {type: String, required: true},
        image: {type: String, required: true}
    }
);
/*productSchema.index({'$**': 'text'});*/
module.exports = mongoose.model("product", productSchema);
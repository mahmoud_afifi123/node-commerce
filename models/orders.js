var mongoose = require('mongoose');
var orderSchema = mongoose.Schema(
    {
        user: {type: Schema.Types.ObjectId, ref: 'User'},
        cart: {type: Object, required: true}
    }
);
module.exports = mongoose.model("Order", orderSchema);
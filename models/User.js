var mongooose = require('mongoose');
var bcyrpt = require('bcrypt-nodejs');
var userSchema = mongooose.Schema(
    {
        email: {type: String, required: true, trim: true},
        password: {type: String, required: true, trim: true},
        fullName: {type: String, trim: true},
        company: {type: String, trim: true},
        job: {type: String, trim: true},
        phoneNumber: {type: Number, trim: true},
        fullAddress: {type: String, trim: true},
        image: {type: String, trim: true},
        followers: [{type: mongooose.Schema.Types.ObjectId, ref: 'User'}],
        followings: [{type: mongooose.Schema.Types.ObjectId, ref: 'User'}],
    }
);
/*userSchema.methods.encryptPassword =function (password) {
 return bcyrpt.hashSync(password, bcyrpt.genSaltSync(5) ,null);
 }

 userSchema.methods.validPassword =function (password) {
 return bcyrpt.compareSync(password, this.password);
 }*/
/*userSchema.methods.findFollowings=functions followings() {
        
}*/
exports.User = mongooose.model('User', userSchema);
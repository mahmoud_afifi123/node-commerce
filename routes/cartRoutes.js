//add to cart route
var Cart=require('../models/Cart');
var path = require('path');
var product=require('../models/product');
module.exports = function (app) {
    app.get('/cart/add/:id', function (req, res) {
        var id = req.params.id;
        var cart = new Cart(req.session.cart ? req.session.cart : {});
        product.findById(id, function (err, product) {
            if (err)
                console.log(err)
            cart.addFunction(product, product.id);
            req.session.cart = cart;
            res.redirect('/home');
        });
    });
//cart route
    app.get('/cart/shoping-cart', function (req, res) {
        if (!req.session.cart) {
            return res.render(path.join(__dirname + '../views/finalCart.ejs'), {allProductsInCart: null});
        }
        var cart = new Cart(req.session.cart);
        res.render(path.join(__dirname, '../views/finalCart.ejs'), {
            allProductsInCart: cart.getAll(),
            totalPrice: cart.totalPrice,
            guest: false
        });
    });
    app.get('/cart/remove/:id', function (req, res) {
        var id = req.params.id;

        function getValues(obj, toFind) {
            var i = 0, key;
            for (key in obj) {
                if (key == toFind) {
                    return i;
                }
                i++;
            }
            return null;
        }

        var index = getValues(req.session.cart.items, id);
    });
}


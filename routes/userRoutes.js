var path = require('path');
var passport = require('passport');
var product = require('../models/product').Product;
var User = require('../models/User').User;
var userController = require('../controllers/UserController');


module.exports = function (app) {
    app.get('/signup', function (req, res) {
        var messages = req.flash('error');
        res.render(path.join(__dirname, '../views/signup.ejs'), {
            messages: messages,
            hasErrors: messages.length > 0,
        });
    });
    app.post('/signup', passport.authenticate('local.signup',
        {
            successRedirect: '/home',
            failureRedirect: '/signup',
            failureFlash: true
        }
    ));

    app.get('/login', function (req, res) {
        var messages = req.flash('error');
        var guest = true;
        res.render(path.join(__dirname, '../views/login.ejs'), {
            messages: messages,
            hasErrors: messages.length > 0,
            guest: guest
        });
    });
    app.post('/login', passport.authenticate('local.signin', {
        successRedirect: '/home',
        failureRedirect: '/login',
        failureFlash: true
    }));

    app.get('/logout', function (req, res, next) {
        req.logout();
        res.redirect('/login');
    });

    app.get('/user/profile/:id', function (req, res, next) {
        User.findById(req.params.id, function (err, user) {
            if (err)
                console.log(err)
            res.render(path.join(__dirname, '../views/overview_profile.ejs'), {user: user});
        });
    });

    app.get('/user/profile/getData/:id', function (req, res) {

        /*var messages = req.flash('error');

        res.render(path.join(__dirname, '../views/editProfile.ejs'), {
            messages: messages,
            hasErrors: messages.length > 0,

        });*/
        User.findById(req.params.id, function (err, user) {
            if (err)
                console.log(err)
            return res.json({user: user})
        });


    });
    app.get('/user/checkFollowing/:id', function (req, res) {

        if (req.user.id != req.params.id) {
            if (req.user.followings.indexOf(req.params.id) != -1) {
                return res.json({message: true})
            }
            else {
                return res.json({message: false})
            }
        }

    });
    app.get('/user/offers/:id', userController.getUserOffers);
    app.post('/user/profile/update', userController.updateProfile);
    app.get('/user/follow/:id', userController.followUser);
    app.get('/user/unfollow/:id', userController.unFollowUser);
    app.get('/user/followings/:id', userController.getFollowings);
    app.get('/user/followers/:id', userController.getFollowers);
    app.get('/user/messages/:id', userController.getMessages);

}
var path = require('path');
var productController = require('../controllers/productController');
module.exports=function (app) {
    app.get('/product/add', function (req, res) {
        res.render(path.join(__dirname,'../views/addProduct'));
    });
    app.post('/product/save', productController.addProduct);
    app.get('/product/:id', productController.viewProduct);
}

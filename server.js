//required modules
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var session = require('express-session');
var sessionMongo = require('connect-mongo')(session);
var csrf = require('csurf');
var User = require('./models/User');
var csrfProtection = csrf();
var passport = require('passport');
require('./config/passport');
var flash = require('connect-flash');
var validator = require('express-validator');
var product = require('./models/product');
var Cart = require('./models/Cart');
var http = require('http').Server(app);
var io = require('socket.io')(http);
const fileUpload = require('express-fileupload');
//
app.use(fileUpload());
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true, uploadDir: './uploads'}));
app.use(validator());

mongoose.connect('mongodb://localhost/buy-it', {useMongoClient: true});

mongoose.Promise = global.Promise;

app.use(session(
    {
        secret: 'mysupersecret',
        resave: false,
        saveUninitialized: false,
        store: new sessionMongo({mongooseConnection: mongoose.connection}),
        cookie: {maxAge: 1000 * 400 * 400}
    }
));
//app.use(csrfProtection);

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());


app.use(function (req, res, next) {
    res.locals.auth = req.isAuthenticated();
    res.locals.session = req.session;
    res.locals.userInfo = req.user;
    next();
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

var usersRoute = require('./routes/userRoutes')(app);
var productRoute = require('./routes/productRoutes')(app);
var cartRoute = require('./routes/cartRoutes')(app);

//home page route

app.get('/home', function (req, res) {
    res.render(__dirname + '/views/home');
});
app.get('/products-list', function (req, res) {
    product.find({}).populate({path: 'user', select: ['_id', 'email', 'fullName']})
        .exec(function (err, result) {
            if (err)
                console.log(err)
            res.json(result);
        });
})
app.get('/product/:id', function (req, res) {
    res.render(__dirname + '/views/one_product');
})
/*io.sockets.on('connection', function (socket) {
    socket.on('send message', function (data) {
        io.sockets.emit('new message', data);
    });
});*/
http.listen(3000);
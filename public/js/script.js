var app = angular.module("instantSearch", []);

function InstantSearchController($scope, $http) {
    $http.get('/products-list').success(function (response) {
        $scope.items = response;
    });
}

function addToCart($scope, $http) {
    $scope.clickFunction = function (id) {
        $http.get("/cart/add/" + id).success(function (response) {
            $scope.items = response;
        });
    };
}




var app = angular.module("profile-module", []);

function profileController($scope, $http ,$location) {
    var id=$location.absUrl().split('/')[$location.absUrl().split('/').length-1]
    $scope.followed = false;
    $scope.checkFollows = function () {
        $http.get('/user/checkFollowing/'+id).success(function (response) {
            $scope.followed = response.message;
        }).error(function (error) {
            console.log(error)
        })
    }

    $scope.checkFollows();
    $scope.getData = function () {
        $http.get('/user/profile/getData/'+id).success(function (response) {
            $scope.data = response.user;
           console.log(response)
        }).error(function (error) {
            console.log(error)
        })
    }
    $scope.isEditing = false;
    $scope.getData();
    $scope.enableEditing = function () {
        $scope.isEditing = !$scope.isEditing;

    }
    $scope.editProfile = function () {
        $http.post('/user/profile/update', $scope.data).success(function (response) {
            $scope.getData();
            $scope.isEditing = false;
        }).error(function (error) {
            console.log(error)
        })
    }
    $scope.unfollowUser=function () {
        $http.get('/user/unfollow/'+id).success(function (response) {
            $scope.getData();
            $scope.followed = false;
        }).error(function (error) {
            console.log(error)
        })
    }
    $scope.followUser=function () {
        $http.get('/user/follow/'+id).success(function (response) {
            $scope.getData();
            $scope.followed = true;
        }).error(function (error) {
            console.log(error)
        })
    }
}

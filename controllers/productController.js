var Product = require('../models/product');
var path = require('path');
exports.addProduct = function (req, res) {
    if (req.files.image) {
        var image = req.files.image;
        var fileNameToSaved = Date.now() + Math.floor((Math.random() * 100) + 1) + image.name;
        image.mv(path.join(__dirname, '../public/uploads/' + fileNameToSaved), function (err) {
            if (err) {
                return res.status(500).send(err);
            }
        });
    }
    var product = new Product({
        name: req.body.name,
        user:req.user,
        price: req.body.price,
        size: req.body.size,
        description: req.body.description,
        image:fileNameToSaved
    });
    product.save(function (err) {
        if (err) return res.status(500).send({message: "Error in adding new product"});
        res.redirect('/home');
    });
}
exports.viewProduct =function (req,res) {
    Product.findById(req.params.id)
        .populate({path: 'followings', select: ['email', 'fullName', 'image']})
        .exec(function (err, product) {
            if (err)
                console.log(err)
            res.render(path.join(__dirname, '../views/one_product.ejs'), {product: product});
        });
}


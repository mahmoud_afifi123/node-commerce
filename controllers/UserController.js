var User = require('../models/User').User;
var Product = require('../models/product');
var path = require('path');
exports.updateProfile = function (req, res) {
    var userId = req.user.id;
    /*if (req.files.image) {
        var image = req.files.image;
        var fileNameToSaved = Date.now() + Math.floor((Math.random() * 100) + 1) + image.name;
        image.mv(path.join(__dirname, '../public/uploads/' + fileNameToSaved), function (err) {
            if (err) {
                return res.status(500).send(err);
            }
        });
    }*/
    var data = {
        fullName: req.body.fullName,
        company: req.body.company,
        job: req.body.job,
        phoneNumber: req.body.phoneNumber,
        fullAddress: req.body.fullAddress,
        /*image: fileNameToSaved || req.user.image*/
    };
    User.findByIdAndUpdate(userId, data, function (err,user) {
        if (err) return res.status(500).send({message: err});
        res.status(200).send({message: "updated sussefully"})
    });

}

exports.followUser = function (req, res) {
    var userId = req.user.id;
    User.findByIdAndUpdate(userId,
        {
            "$push": {followings: req.params.id}
        }, function (err) {
            if (err) {
                return res.status(500).send({message: "Error in following person"});
            }
            else {
                User.findByIdAndUpdate(req.params.id,
                    {
                        "$push": {followers: req.user.id}
                    }, function (err) {
                        if (err) {
                            return res.status(500).send({message: "Error in follower person"});
                        }
                        else {
                            res.json({message:'followed'});
                        }
                    });


            }
        });
}

exports.unFollowUser = function (req, res) {
    var userId = req.user.id;
    User.findByIdAndUpdate(userId,
        {
            "$pop": {followings: req.params.id}
        }, function (err) {
            if (err) {
                return res.status(500).send({message: "Error in following person"});
            }
            else {
                User.findByIdAndUpdate(req.params.id,
                    {
                        "$pop": {followers: req.user.id}
                    }, function (err) {
                        if (err) {
                            return res.status(500).send({message: "Error in follower person"});
                        }
                        else {
                            res.json({message:'unfollowed'});
                        }
                    });


            }
        });
}
exports.getFollowings = function (req, res) {
    User.findById(req.params.id)
        .populate({path: 'followings', select: ['email', 'fullName', 'image', 'job']})
        .exec(function (err, user) {
            if (err)
                console.log(err)
            res.render(path.join(__dirname, '../views/followings.ejs'), {user: user});
        });
}
exports.getFollowers = function (req, res) {
    User.findById(req.params.id)
        .populate({path: 'followers', select: ['email', 'fullName', 'image']})
        .exec(function (err, user) {
            if (err)
                console.log(err)
            res.render(path.join(__dirname, '../views/followers.ejs'), {user: user});
        });
}
exports.getMessages = function (req, res) {
    User.findById(req.params.id)
        .populate({path: 'followers', select: ['email', 'fullName', 'image']})
        .exec(function (err, user) {
            if (err)
                console.log(err)
            res.render(path.join(__dirname, '../views/chat_area'), {user: user});
        });
}
exports.getUserOffers = function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err)
            return res.end(err)
        Product.find({user: req.params.id}).populate({path: 'user', select: ['id']}).exec(function (err, result) {
            if (err)
                return res.end(err)
            res.render(path.join(__dirname, '../views/user_offers.ejs'), {allProducts: result, user: user});
        });

    });
}


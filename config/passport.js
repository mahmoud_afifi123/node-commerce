var passport = require('passport');
var User = require('../models/User').User;
var localStrategy = require('passport-local').Strategy;

passport.serializeUser(function (user, done) {
    done(null, user.id);
});
passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});
passport.use('local.signup', new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, email, password, done) {
    req.checkBody('email','Invalid email').notEmpty().isEmail();
    req.checkBody('password','Invalid password').notEmpty().isLength({min:4});
    var errors=req.validationErrors();
    if (errors)
    {
        var messages=[];
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        return done(null,false,req.flash('error',messages.mess));
    }
    User.findOne({'email': email}, function (err, user) {
        if (err) {
            return done(err);
        }
        if (user)
        {
            return done(null,false,{ message:'this email already in use'});
        }
        var newUser= new User();
        newUser.email=email;
        //newUser.password=newUser.encryptPassword(passport)
        newUser.password=password;
        newUser.save(function (err,result) {
            if(err)
            {
                return done(err);
            }
            return done(null,newUser);
        });
    });
}));
passport.use('local.signin',new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
},function(req,email,password,done)
{
    req.checkBody('email','Invalid email').notEmpty().isEmail();
    req.checkBody('password','Invalid password').notEmpty();
    var errors=req.validationErrors();
    if (errors)
    {
        var messages=[];
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        return done(null,false,req.flash('error',messages.mess));
    }
    User.findOne({'email': email}, function (err, user) {
        if (err) {
            return done(err);
        }
        if (!user)
        {
            return done(null,false,{ message:'No user found'});
        }
        if(user.password != password)
        {
            return done(null,false,{ message:'Wrong password, Please check it again'});
        }
        return done(null,user);
    });
}));